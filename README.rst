Lplug
=====

Installation
************

**NOTE**:
If you have both Python 2 and 3 installed in your system,
you probably need use pip3 instead of pip.

If you want lplug without external integrations: :code:`pip install lplug` .

.. Or, if you want all the integration: :code:`pip install lplug[all]`.

.. More informations about the installation process at:
.. `<https://prettyetc.gitlab.io/lplug/developing/userguide/installation.html>`_


Docs
****

For a detailed explanation of Lplug features,
API reference and other, we suggest to go to the documentation of Lplug at
`<https://prettyetc.gitlab.io/lplug/index.html>`_
