API User Guide
==============

Table of contents
-----------------

.. toctree::
    :maxdepth: 1

    .. getting started
    installation
    quick start

    .. advanced usage
    .. TODO: Create them
    .. custom managers
    .. packaging
