Installation
============

Lplug requires at least Python 3.4 and works with both CPython and PyPy.


Quick install (pip)
-------------------

The lplug package is available on PyPI as "lplug".

.. code-block:: bash

    pip install lplug

.. The basic installation offers only the core features of lplug,
.. excluding all the extras that depends on third party packages.
.. if you want all of them, you should use the `all` extra as follows.
..
.. .. code-block:: bash
..     pip install lplug[all]
..
.. All the extra dependencies are defined
.. `here <https://gitlab.com/prettyetc/lplug/-/blob/master/setup.py>`_.

.. .. seealso::
..     :ref:`Extra managers` for a detailed reference of extras.


Installation from source
------------------------

We recommend the installation from pip, if it is not possible,
you can install it manually or use it directly from the project root.

If you want to install it from source, you will need to run the `setup.py` script
(it requires setuptools) as follows.

.. code-block:: bash

    python setup.py install

or

.. code-block:: bash

    pip install -e .
