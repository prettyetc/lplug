Quick start
===========

This page introduces you to the lplug managers and some lplug specific concepts.


Overview of the lplug plugin system
***********************************

Lplug managers allows you to collect plugins from different sources handling all the issues for you.
Plugins can be any kind of Python object with no restrictions,
from numbers to containers of any kind, including the mutable ones,
or even user defined classes and instances of them.
Unless it is forbidden by a specific manager, lplug managers can have duplicated plugins.
Plugin managers can specify some attributes for each plugins,
allowing a simple plugin classification through those attributes.

Under the hood, each plugin is identified by an :class:`int` id (they are called uid in the docs)
and each plugin has a :class:`~.BaseManager.PluginSpec` with it.

.. note::
    Lplug managers does not really manage plugin automatically
    (e.g. instancing and/or calling methods to them),
    but they are more like plugin collectors
    and storages with a simple, but flexible, classification.

.. note::
    Each uid is generated by :func:`uuid.uuid4`,
    but they are converted to :class:`int` for performance.
    If you want to use :class:`~.uuid.UUID` you can pass the :class:`int` uid to the
    :class:`~.uuid.UUID` constructor like this: :code:`UUID(int=uid)` .
    Almost all managers' methods accept both :class:`int`\s and :class:`~.uuid.UUID`\s for uids.


The :class:`~.PluginManager` class: first steps
***********************************************

If you don't plan to create your own manager or use one of the extras,
:class:`~.PluginManager` is the best choise.
:class:`~.PluginManager` offers a general purpose and module oriented plugin manager
with classes in mind.

So, let's see an application of this class (because you are here to see some code ;) ).
First of all, you need an instance of :class:`~.PluginManager`.

.. code-block:: python

    from lplug import PluginManager

    manager = PluginManager()

The manager can be customized using several settings that affects its behaviour.
Here are listed some options that we use for this guide.
Settings can be passed to :class:`~.PluginManager` constructor or by setting fields
to :attr:`~.BaseManager.settings` attribute.

baselist
    This parameter accepts a list of types that are used to filter and classified types.
    You can use it to collect only subclasses of your plugin base class.

namelist
    This parameter accepts a list of names that are used to filter and classified plugins.
    You can use it to select only plugins that have specific names.
    This description can create some confusion now, it will explained better with an example.

extra_predicate
    This parameter accept a callable that accept 3 parameter, module, name and the plugin object.
    It can be used for adding checks to plugins without subclassing.

.. seealso::
    :class:`~.PluginManager` and :class:`~.BaseManager` documentation
    for more information about settings.


Then let's define a plugin:

.. code-block:: python

    plugin = "That's a plugin."

And add it to the manager previously created.

.. code-block:: python

    uid = manager.load_plugin(plugin, "plugin")

Voilà! You have loaded your first plugin! And it's a string.
You can get the plugin using :meth:`~.BaseManager.get_plugin_by_id`.
Before stepping over, here is the whole code:

.. code-block:: python

    from lplug import PluginManager

    plugin = "That's a plugin."

    manager = PluginManager()

    uid = manager.load_plugin(plugin, "plugin")
    print(manager.get_plugin_by_id(uid))  # That's a plugin.


.. tip::
    The same result can be achieved with the :class:`~.BaseManager` class.

The :class:`~.PluginManager` class: type classification
*******************************************************

Now, let's see some classification. For this section classes are used.

In this example there are 2 base classes, one for parsing data contained in a string
and one for serializing it to a string.

.. code-block:: python

    # api.py

    class BaseParserPlugin(object):
        def parse(self, data):
            """Parse some data."""
            raise NotImplementedError()

    class BaseSerializerPlugin(object):
        def serialize(self, obj):
            """Serialize an object."""
            raise NotImplementedError()


Now, these classes can be imported by any module to make plugins, like these:

.. code-block:: python

    # plugins/int_parse.py

    from api import BaseParserPlugin

    __all__ = ("IntParserPlugin",)

    class IntParserPlugin(BaseParserPlugin):
        def parse(self, data):
            try:
                return int(data)
            except ValueError:
                return None

.. code-block:: python

    # plugins/int_serialize.py

    from api import BaseSerializerPlugin

    __all__ = ("IntSerializerPlugin",)

    class IntSerializerPlugin(BaseSerializerPlugin):
        def serialize(self, obj):
            return str(obj)

.. code-block:: python

    # plugins/float_parse_serialize.py

    from api import BaseParserPlugin, BaseSerializerPlugin

    __all__ = ("FloatParserPlugin", "FloatSerializerPlugin")

    class FloatParserPlugin(BaseParserPlugin):
        def parse(self, data):
            try:
                return float(data)
            except ValueError:
                return None

    class FloatSerializerPlugin(BaseSerializerPlugin):
        def serialize(self, obj):
            return str(obj)

After defining an API and some plugins, now you can start classifying plugins by type.
Loaded plugins are automatically sorted by the manager for you.
The only thing you need to specify is the :class:`~.PluginManager`'s `baselist` setting.
After loading plugins you can use :meth:`~.BaseManager.get_plugins_by_category_key` to get plugins,
like in the example below.

.. code-block:: python

    # manager.py

    from lplug import PluginManager

    # Import api
    from api import BaseParserPlugin, BaseSerializerPlugin

    # Import plugin
    from plugin.int_parse import IntParserPlugin
    from plugin.int_serialize import IntSerializerPlugin
    from plugin.float_parse_serialize import FloatParserPlugin, FloatSerializerPlugin

    # Create the manager
    manager = PluginManager(baselist=(BaseParserPlugin, BaseSerializerPlugin))

    # Another way to set baselist
    # manager.settings.baselist = (BaseParserPlugin, BaseSerializerPlugin)

    # Load the plugins (the order doesn't matter!)
    manager.load_plugin(IntParserPlugin)
    manager.load_plugin(IntSerializerPlugin)
    manager.load_plugin(FloatParserPlugin)
    manager.load_plugin(FloatSerializerPlugin)

    print(manager.get_plugins_by_category_key("base", BaseParserPlugin))
    #   (<class 'IntParserPlugin'>, <class 'FloatParserPlugin'>)

    print(manager.get_plugins_by_category_key("base", BaseSerializerPlugin))
    #   (<class 'IntSerializerPlugin'>, <class 'FloatSerializerPlugin'>)


.. warning::
    Modify the `baselist` setting (or any other setting) after load does nothing as settings are read
    only at load time. So, if you want to sort plugin using a different criteria, you need to reload
    them. Currently, there is no options to do it in place.

.. hint::

    Even if it is the safest option, manual import of plugins is usually not done because you can't
    import external plugins as they must be loaded dynamically. Lplug allows you to load plugins
    given paths to module or folders (packages) in the safest and trasparent manner,
    but this is discussed later.


The :class:`~.PluginManager` class: name classification
*******************************************************

Similarly to what was done with bases, :class:`~.PluginManager` has a setting called `namelist`
that allows you to specify a list of names for classifying.

Using again the parser/serializer example defined above,
let's see an example of name classification:

.. code-block:: python

    # manager2.py

    from lplug import PluginManager

    # Import plugin
    from plugin.int_parse import IntParserPlugin
    from plugin.int_serialize import IntSerializerPlugin
    from plugin.float_parse_serialize import FloatParserPlugin, FloatSerializerPlugin

    # Create the manager
    manager = PluginManager(namelist=("parser", "serializer"))
    # Load the plugins (the order doesn't matter!)
    manager.load_plugin(IntParserPlugin, "parser")
    manager.load_plugin(IntSerializerPlugin, "serializer")
    manager.load_plugin(FloatParserPlugin, "parser")
    manager.load_plugin(FloatSerializerPlugin, "serializer")

    print(manager.get_plugins_by_category_key("name", "parser"))
    #   (<class 'IntParserPlugin'>, <class 'FloatParserPlugin'>)

    print(manager.get_plugins_by_category_key("base", "serializer"))
    #   (<class 'IntSerializerPlugin'>, <class 'FloatSerializerPlugin'>)

.. note::
    This example is a bit hacky because the name is specified in the `attrname` parameter.
    Usually that parameter is used when loading plugins from modules.

Module and package loading
**************************

One of the strength of lplug is the ability to scan module objects to get plugins and load modules
from different sources. lplug provides different ways to load plugins with or without modules,
each one with different flexibility and control. Here is a list of available methods:

- :meth:`~.BaseManager.load_plugin`.
 This is the most flexible way that give the control on almost everything about loading
 (even the uid). It is used by *all* the other methods to load plugins that they found.

- :meth:`~.BaseManager.fetch_module`
 This fetches a module object (including the ones created with :class:`types.ModuleType`),
 to get plugins. By default, it uses `__all__` to get a list of public members of module.
 A limitation of it is that you can't know which plugins are loaded and which not.
 It is used by *all* the other methods to load plugins from modules that they found.

- :meth:`~.BaseManager.load_module`
 This loads a module given its path and it works with folders too.
 An interesting feature of it is that it removes the loaded module from the Python import system,
 so it can be reloaded if it changes.

- :class:`~.SearchManager` and :attr:`.PluginManager.searcher`
 :class:`~.SearchManager` contains some methods to scan folder and they return a
 list of paths to give to :meth:`~.BaseManager.load_module` and :meth:`~.BaseManager.load_modules`.
 :attr:`.PluginManager.searcher` is an instance of :class:`~.SearchManager`.

Here is an example of the last method (using the parser/serializer example):

.. code-block:: python

    # manager3.py

    import os.path

    from lplug import PluginManager

    from api import BaseParserPlugin, BaseSerializerPlugin

    manager = PluginManager(baselist=(BaseParserPlugin, BaseSerializerPlugin))

    paths = manager.searcher.fetch_folders_pkgutil(os.path.abspath("plugins/"))
    manager.load_modules(*paths)

    print(manager.get_plugins_by_category_key("name", "parser"))
    #   (<class 'IntParserPlugin'>, <class 'FloatParserPlugin'>)

    print(manager.get_plugins_by_category_key("base", "serializer"))
    #   (<class 'IntSerializerPlugin'>, <class 'FloatSerializerPlugin'>)

.. note::
    At the moment only pure modules are supported (.py, .pyc files and folders must
    have an `__init__.py`). So, no zip packages or other kind of modules are supported natively.
    Luckily, modules are universally managed by the import system (and globally stored in
    :data:`sys.modules`), so modules produced by "unsupported" sources can be loaded in lplug
    managers through :meth:`~.BaseManager.fetch_module`.
    The only limitation is that they should look like "normal" modules (currently, it is not forced).

Final notes
***********

This guide illustrates you the main features of lplug and its managers.
While lots of things was explained here, it is always suggested to go to the API documentation for
more information about :class:`~.PluginManager` and others.
