Core members
============

.. automodule:: lplug.base
   :members:
   :show-inheritance:
   :special-members:

.. autoclass:: lplug.manager.PluginManager
   :members:
   :show-inheritance:
   :special-members:

.. automodule:: lplug.managers
   :members:
   :exclude-members: PluginManager
   :show-inheritance:
   :special-members:
