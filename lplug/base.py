#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""All the basic stuffs to start with plugin processing."""

from collections.abc import Iterable as IterableType
import importlib
import itertools
import logging
import os
import random
import sys
import types
import uuid
from typing import (
    Dict,
    Hashable,
    Iterable,
    Optional,
    Sequence,
    Tuple,
    Union,
    Mapping,
    Set,
)

__all__ = ("namedlist", "BaseManager")

modlogger = logging.getLogger(__name__)

_HAS_REFCOUNT = hasattr(sys, "getrefcount")
_default_predicate = lambda *_: True
_IS_PY34 = sys.version_info < (3, 5)

# Typing
if sys.version_info < (3, 6):
    ModuleNotFoundError = ImportError  # pylint: disable=W0622
    PathLike = str
else:
    PathLike = Union[str, os.PathLike]


class namedlist(object):  # pylint: disable=C0103
    """
    A class that behave similar to :class:`collections.namedtuple`,
    except that fields are specified in :data:`~object.__slots__`
    and attributes are editable.

    It supports inhiterance to specify other attribute in `__slots__`.
    """

    __slots__ = ()

    @classmethod
    def create(cls, name: str, *attrs: str) -> type:
        """
        Create a new :class:`namedlist` type with the given attributes.

        :param name: The classname to pass to :class:`type`.

        :param *attrs: The members of the class.
                       They are used to construct :data:`~object.__slots__`.
        :return: The generated subclass.
        """
        return type(name, (cls,), {"__slots__": attrs})

    def __init__(self, **kwargs: Dict[str, object]):
        super().__init__()

        for key, val in kwargs.items():
            setattr(self, key, val)

    def __repr__(self) -> str:
        """Represent self with types for attributes."""
        return (
            "{}(".format(type(self).__name__)
            + ",".join(
                "{} = {}".format(
                    name,
                    type(obj).__name__,
                )
                for name, obj in self
            )
            + ")"
        )

    def __getattribute__(self, name):  # pylint: disable: W0235
        # Suppress pylint errors
        return super().__getattribute__(name)

    def __getitem__(self, key: str) -> object:
        """Get given setting key."""
        return getattr(self, key)

    def __setitem__(self, key: str, val: object) -> None:
        """Set given setting key."""
        setattr(self, key, val)

    def __eq__(self, other: Union["namedlist", Mapping]) -> bool:
        """Compare self with a :class:`namedlist` or with a mapping."""
        if len(self) == len(other):
            for (key, val) in self:
                if other[key] != val:
                    return False
            return True
        return False

    def __iter__(self) -> Iterable[Tuple[str, object]]:
        """Iterate over each valid setting pair."""
        # Iterate over each field in every __slots__ attribute for each class,
        # if it has, in the mro.
        # The iterator return each attribute with
        # its value if the attribute exists.
        return iter(
            (attr, getattr(self, attr)) for attr in self._attrs if hasattr(self, attr)
        )

    def __len__(self) -> int:
        """Get the number of *unique* fields."""
        return sum(1 for _ in self._attrs)

    @property
    def _attrs(self) -> Set[str]:
        """
        Get all the defined attributes.

        :return: All the attributes specified in each `__slots__`.

        :meta public:
        """
        return frozenset(self._get_all_slots())

    def _get_all_slots(self) -> Iterable[str]:
        """Get all members in every __slots__ attribute in the mro."""
        return itertools.chain(
            *(x.__slots__ for x in type(self).mro() if hasattr(x, "__slots__"))
        )


# TODO: make the manager thread safe
# TODO: make a working copy method.
class BaseManager(object):
    """
    Manage plugins and them loading/unloading.

    It fetches modules and directories to find valid plugins,
    then registers and saves it.

    It can be configured using the :attr:`~BaseManager.settings` attribute,
    or by passing the settings keys through the constructor.

    :param bool use_all:
        If True, it gets module attributes from :code:`__all__`,
        or what is specified by :attr:`all_name`,
        otherwise it gets module attribute via :func:`dir`.
        The default is True.
        If :code:`__all__` in module does not exists or it is not valid,
        :func:`dir` is used as fallback.

    :param str all_name: Specify what attribute names
                         should be use for getting module members.
                         The default is :code:`"__all__"`.

    :param bool collect_empty_modules:
        If True, it saves modules that have no valid plugins,
        otherwise empty modules are skipped. The default is False.

    :param bool only_public:
            If True, it does not load plugins that have
            name that starts with `_`,
            otherwise all the module members are loaded regardless the name.
            The default is True.

    :param extra_predicate:
        A callback that takes a 3 parameters:

        - The module instance (can be None);
        - The plugin name (can be None);
        - and the plugin object;

        It returns True if the plugin is valid, False otherwise.
        The default callback always returns True.


    :type extra_predicate: Callable[
            [Optional[types.ModuleType], Optional[str], object],
            bool
        ]

    .. seealso::
        `The Python import system
        <https://docs.python.org/3/reference/import.html>`_.
    """

    # namedtuples
    PluginSpec = namedlist.create("PluginSpec")
    Settings = namedlist.create(
        # collector settings
        "Settings",
        "use_all",
        "all_name",
        "collect_empty_modules",
        # "suppress_error",
        # filter settings
        "only_public",
        "extra_predicate",
    )

    logger = modlogger.getChild("manager")

    __slots__ = (
        # "_lock",
        "settings",
        "loaded_objects",
        "attribute_container",
    )

    def __init__(self, **settings: object):
        super().__init__()
        _default = {
            "use_all": True,
            "all_name": "__all__",
            "only_public": True,
            # "suppress_error": True,
            "collect_empty_modules": False,
            "extra_predicate": _default_predicate,
        }
        _default.update(**settings)
        self.settings = self.Settings(**_default)

        self.loaded_objects = {}
        self.attribute_container = self.PluginSpec(
            **{key: {} for key in self.PluginSpec()._attrs}
        )

    # Protected method
    def _insert_id_into(
        self, uid: Union[int, uuid.UUID], category: str, key: object
    ) -> None:
        r"""
        Insert a plugin into the specified category group.

        The inserted plugin will be available in the collection returned by
        :meth:`~BaseManager.get_plugins_by_category_key`

        :param int uid: The plugin uid
        :param str category: The category where the plugin should be placed.
        :param object key: The key group of the category.

        :raises AttributeError: If the given category does not exists.
        :raises KeyError: If the given uid is not registered.

        .. warning::
            This method should be called only by
            :meth:`~BaseManager.load_plugin` and not outside.

        :meta public:
        """
        if isinstance(uid, uuid.UUID):
            uid = uid.int

        _category = getattr(self.attribute_container, category, ...)
        if _category == ...:
            raise AttributeError(
                "Category {} can't be used "
                "because it is not defined.".format(category)
            )

        self.loaded_objects[uid][1][category] = key

        if key in _category:
            _category[key].append(uid)
        else:
            _category[key] = [uid]

    # Getters
    def get_plugin_by_id(
        self, uid: Union[int, uuid.UUID], return_spec: bool = False
    ) -> object:
        """
        Get a specific plugin given its id.

        :param int uid: The uid that identifies the plugin.

        :param bool return_spec: If True, returns a tuple with the plugin
                                 and its specs,
                                 otherwise returns only the plugin.
        :return: The plugin identified by the uid.

        :raises KeyError: If the uid does not esists.
        """
        if isinstance(uid, uuid.UUID):
            uid = uid.int

        plugin = self.loaded_objects[uid]

        if return_spec:
            return plugin
        return plugin[0]

    def get_plugins_by_category_key(
        self, category_name: str, key: Hashable
    ) -> Sequence[object]:
        """
        Get a list of plugins by category and its key.

        :param str category_name: The category to get.
            Must be defined as :attr:`~BaseManager.PluginSpec`.
        :param object key: The key to select in the category.
            The object must be hashable.

        :return: A collection of plugins.
                 Can be empty if there are no elements with the given key.

        :rtype: Sequence[object]

        :raise AttributeError: If the category does not exists.
        """
        category = getattr(self.attribute_container, category_name, None)
        if category is None:
            raise AttributeError(
                "Category {} can't be used "
                "because it is not defined.".format(category_name)
            )
        objects = category.get(key, ())

        if isinstance(objects, IterableType) and not isinstance(
            objects, (str, bytes, bytearray)
        ):
            return tuple(self.loaded_objects[x][0] for x in objects)

        # If the container saves only one object per key.
        return (self.loaded_objects[objects][0],)

    # Plugin loaders
    def load_plugin(
        self,
        plugin: object,
        name: Optional[str] = None,
        module: Optional[types.ModuleType] = None,
        uid: Union[int, uuid.UUID, None] = None,
    ) -> Optional[int]:
        """
        Register a new plugin class and save it into the right category.

        This method is responsible to check if the plugin can be loaded or not.

        :param plugin: The object to be registered.

        :param name: The plugin name. Usually, it is the name in the module.

        :param module: The module where plugin came from.

        :param uid: The plugin UUID.
                    This parameter allows to check
                    if the plugin is already registered.

                    Defaults to None.

        :return: The plugin UUID or None if the plugin can not be loaded.
        """
        settings = self.settings

        if isinstance(uid, uuid.UUID):
            uid = uid.int

        if name is None and isinstance(plugin, type):
            name = plugin.__name__

        if uid not in self.loaded_objects and settings.extra_predicate(
            module, name, plugin
        ):
            uid = self._register_object(plugin, {}, uid=uid)
        return uid

    def fetch_module(self, mod: types.ModuleType, path: str = None) -> bool:
        """
        Fetch a module for searching plugins.

        :param mod: The module where looking for plugins

        :param path: If specified, the absolute path to the module.
                     If not specified, the path is taken from
                     the module :attr:`__file__` attribute.

        :return: True if the module contains at least 1 valid plugin,
                 False otherwise.
        """
        settings = self.settings
        all_name = settings.all_name

        # Collect module attributes
        if settings.use_all:
            attrs = getattr(mod, all_name, None)
            if not isinstance(attrs, IterableType):
                # Fallback to dir()
                self.logger.warning(
                    "Module %s has not the attribute %s, fallback to dir()",
                    mod.__name__,
                    all_name,
                )
                attrs = dir(mod)
        else:
            attrs = dir(mod)

        if path is None:
            path = getattr(mod, "__file__", mod.__name__)

        if settings.only_public:
            # This filter is used even if attrs are god from __all__.
            attrs = filter(
                lambda name: not name.startswith("_"),
                attrs,
            )

        mod_members = []
        for attrname in attrs:
            if not hasattr(mod, attrname):
                # This happens only with __all__
                self.logger.debug("Attribute %s isn't exists", attrname)
                continue

            attr = getattr(mod, attrname)
            uid = self.load_plugin(attr, attrname, module=mod)
            if uid:
                mod_members.append(uid)
                self.logger.info(
                    "Found plugin on %s typed %s",
                    attrname,
                    type(attr).__name__,
                )
            else:
                self.logger.debug(
                    "Skip attribute %s typed %s on module %s",
                    attr,
                    type(attr).__name__,
                    path,
                )

        # Check if one plugin was loaded at least
        if not (mod_members or settings.collect_empty_modules):
            self.logger.warning("Module %s has no valid plugins.", path)
            return False

        return True

    def load_module(
        self, path: Union[str, types.ModuleType], is_folder: bool = False
    ) -> bool:
        """
        Load a module by path.

        :param path: The absolute path to the module.

            You can pass a module object, too.

            Note that if you pass a module,
            it will be removed from the import cache.
            If you don't want that behaviour,
            use :meth:`~BaseManager.fetch_module` instead.

        :param bool is_folder: If True, the module is considered a package.
            The folder must be valid in the file system
            and must contain an `__init__.py` file with the plugins to expose.
            This parameter is ignored if the path parameter is a module.

        :return: True if the module is loaded correcly and
                 contains at least 1 plugin, False otherwise.

        :raises TypeError: If the given path is invalid.
        :raises ModuleNotFoundError:
            If the given path does not point to a valid Python module.
        """
        if isinstance(path, str):
            _, module_file = os.path.split(path)
            modname, _ = os.path.splitext(module_file)
            # Add some noise to modname helps to
            # reduce a race condition if the module name
            # is the same across several load_module calls or several managers.
            # But this does not remove completely the issue because
            # the module storage is just one and it is global
            modname = "lplug{}_{}".format(
                random.randint(10000, 99999),
                modname,
            )

            module_path = path

            if is_folder:
                # Only files can be loaded,
                # but spec_from_file_location should know that is a package
                module_path = os.path.join(module_path, "__init__.py")

                spec = importlib.util.spec_from_file_location(
                    modname, module_path, submodule_search_locations=[]
                )
            else:
                spec = importlib.util.spec_from_file_location(
                    modname,
                    module_path,
                )

            if spec is None:
                raise ModuleNotFoundError(
                    "File {} is not a valid module".format(module_path)
                )

            try:
                if hasattr(spec.loader, "create_module"):
                    # Use the supported create_module
                    if _IS_PY34:
                        # SourceFileLoader on py3.4 does not support
                        # create_module, so this branch is probably never used.
                        mod = spec.loader.create_module(spec)
                        if mod is None:
                            mod = types.ModuleType(modname)
                    else:
                        # This is the preferred way to create a module
                        mod = importlib.util.module_from_spec(spec)
                    if is_folder:
                        # Register a temporary module for relative imports
                        sys.modules[mod.__name__] = mod
                        spec.loader.exec_module(mod)
                        del sys.modules[mod.__name__]
                    else:
                        spec.loader.exec_module(mod)
                else:
                    # Fallback to legacy load_module
                    mod = spec.loader.load_module()

            except Exception as ex:  # pylint: disable=W0703
                # TODO: report the exception raised by the module
                self.logger.error(
                    "Failed to load module %s from %s",
                    modname,
                    path,
                    exc_info=ex,
                )
                return False

            self.logger.debug(
                "Load %s from %s",
                "package" if is_folder else "module",
                path,
            )

        elif isinstance(path, types.ModuleType):
            mod = path
            modname = mod.__name__

        else:
            raise TypeError(
                "Invalid path typed {}".format(type(path).__name__),
            )

        valid = self.fetch_module(mod)
        self._remove_mod_refs(mod, modname, recursive=is_folder)
        return valid

    def load_modules(self, *paths: PathLike) -> None:
        """Load all the modules from given paths."""
        for path in paths:
            self.load_module(
                path,
                is_folder=not isinstance(path, types.ModuleType)
                and os.path.isdir(path),
            )

    # Plugin removes
    def remove_plugin_by_id(self, uid: Union[int, uuid.UUID, None]) -> bool:
        """
        Remove a plugin from the index, given its UUID.

        :param uid: The plugin UUID.

        :return: True if the UUID was existed in the index, False otherise.
        """
        if isinstance(uid, uuid.UUID):
            uid = uid.int

        if uid is None or uid not in self.loaded_objects:
            return False

        _, attrs = self.loaded_objects[uid]
        del self.loaded_objects[uid]

        for category, value in attrs:
            uid_list = getattr(self.attribute_container, category)[value]
            uid_list.remove(uid)
            if not uid_list:
                del getattr(self.attribute_container, category)[value]
        return True

    def remove_plugin(
        self,
        plugin: object,
        use_identity: bool = False,
    ) -> bool:
        """
        Remove a plugin from the index.

        :param plugin: The plugin to remove.

        :param use_identity:
            If True, plugins are compared using the
            :any:`Identity comparison <python:is>`,
            otheriwse they are compared normally with :code:`==`.

            This parameter is useful for literal comparison.

        :return: True if the plugin was registered, False otherise.
        """
        uid_to_remove = None
        if use_identity:
            predicate = lambda pair: pair[0] is plugin
        else:
            predicate = lambda pair: pair[0] == plugin

        for (uid, pluginpair) in self.loaded_objects.items():
            if predicate(pluginpair):
                uid_to_remove = uid
                break

        return self.remove_plugin_by_id(uid_to_remove)

    def remove_plugins(
        self, plugins: Iterable[object], use_identity: bool = False
    ) -> bool:
        """
        Remove plugins from the index.

        :param plugins: A collection of plugins to remove.

        :param use_identity:
            If True, plugins are compared using the
            :any:`Identity comparison <python:is>`,
            otheriwse they are compared normally with :code:`==`.

            This parameter is useful for literal comparison.

        :return: True if all the plugins were found and deleted, False otherise.
        """
        plugins = list(plugins)
        uids_to_remove = []
        if use_identity:
            predicate = lambda pair, plugin: pair[0] is plugin
        else:
            predicate = lambda pair, plugin: pair[0] == plugin

        for (uid, pluginpair) in self.loaded_objects.items():
            plugin_to_remove = None
            for i, plugin in enumerate(plugins):
                if predicate(pluginpair, plugin):
                    plugin_to_remove = i
                    break

            if plugin_to_remove is not None:
                del plugins[plugin_to_remove]
                uids_to_remove.append(uid)

        if not uids_to_remove:
            return False

        for uid in uids_to_remove:
            self.remove_plugin_by_id(uid)
        return True

    # Internal methods
    def _register_object(
        self, obj: object, attrs: Dict[str, object], uid: Optional[int]
    ) -> int:
        """Create an UUID for the object and save it into loaded objects."""
        if uid is None:
            uid = uuid.uuid4().int

            while uid in self.loaded_objects:
                uid = uuid.uuid4().int

        self.loaded_objects[uid] = (obj, self.PluginSpec(**attrs))
        return uid

    def _remove_mod_refs(
        self,
        mod: Optional[types.ModuleType],
        modname: str,
        recursive: bool = False,
    ) -> None:
        """Remove object reference to the module."""
        if (
            self.logger.isEnabledFor(logging.DEBUG)
            and _HAS_REFCOUNT
            and mod is not None
        ):
            refcount = sys.getrefcount(mod)
            self.logger.debug(
                "References to %s: %s",
                modname,
                refcount - 3 if refcount > 2 else "/",
            )

        # if module_path in sys.path_importer_cache:
        #     self.logger.debug(
        #         "Found a cached module's path, this will be deleted")
        #     del sys.path_importer_cache[module_path]
        # importlib.invalidate_caches()
        if recursive:
            for name in tuple(x for x in sys.modules if x.startswith(modname)):
                del sys.modules[name]

        elif modname in sys.modules:
            del sys.modules[modname]
