#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""A collection of plugin managers."""

import gc
import pkgutil
import logging
import site
import os
import sys
import types
from typing import Any, Iterable, Optional, Union

import pkg_resources

from .base import BaseManager, namedlist

__all__ = ("SearchManager", "PluginManager")

_HAS_REFCOUNT = hasattr(sys, "getrefcount")
modlogger = logging.getLogger(__name__)
_default_predicate = lambda *_: True


class SearchManager(BaseManager):
    """
    Load and manage module searchers.

    Each searcher should have at least 3 arguments:

    :param Iterable[str] args:
        An iterable of str that points to modules positions.
        It usually contains file paths.
    :param bool no_pkg:
        If True, packages (modules contained in a directory or `__init__.py`)
        are skipped, otherwise they are collected as modules.
        Some searchers can ignore this parameter if they can't know
        if the module is a package.
    :param bool skip_controls:
        If True, any module validation (including the ones done if no_pkg is True)
        will be skipped, otherwise checks are done where possible.
        The default is False.

    :return: A list of absolute paths or module objects.
    :rtype: Iterable[Union[str, types.ModuleType]]
    """

    PluginSpec = namedlist.create("PluginSpec", "module", "name")
    logger = modlogger.getChild("searcher")

    # Searches
    @classmethod
    def fetch_folders_simple(
        cls,
        paths: Iterable[str],
        no_pkg: bool = False,
        skip_controls: bool = False,
    ) -> Iterable[str]:
        """
        Fetch folders for finding plugin modules.

        .. tip::
            You should prefer :meth:`~SearchManager.fetch_folders_pkgutil`
            over this method.

        .. note::
            It doesn't go into subdirectories.
        """
        if not skip_controls:
            # Initialize controls
            file_checks = (
                lambda fname: not fname.startswith("_"),
                lambda fname: fname.endswith(".py"),
            )
            dir_checks = (
                lambda dirname: dirname != "__pycache__",
                lambda dirname: os.path.isfile(
                    os.path.join(dirname, "__init__.py"),
                ),
            )

        modules = []
        for path in paths:
            path = os.path.abspath(path)
            if not os.path.isdir(path):
                continue
            result = os.listdir(path)
            cls.logger.debug("Found files/directories in %s: %s", path, result)

            if not skip_controls:
                _trash_res = []
                for modpath in result:
                    absmodpath = os.path.join(path, modpath)
                    if os.path.isfile(absmodpath):
                        checks = file_checks
                    elif not no_pkg and os.path.isdir(absmodpath):
                        checks = dir_checks
                    else:
                        _trash_res.append(modpath)
                        continue
                    if not all(check(modpath) for check in checks):
                        _trash_res.append(modpath)

                for trash in _trash_res:
                    result.remove(trash)

            cls.logger.info(
                "Found %s %s in %s",
                len(result),
                "modules" if no_pkg else "modules/packages",
                path,
            )

            modules.extend(os.path.join(path, x) for x in result)
        return modules

    @classmethod
    def fetch_folders_pkgutil(
        cls,
        paths: Iterable[str],
        no_pkg: bool = False,
        skip_controls: bool = False,
        begin: str = "",
        end: str = "",
    ) -> Iterable[str]:
        """
        Fetch folders for finding plugin modules, using :mod:`pkgutil`.

        :param str begin: Required begin of the module name.
                          If the given str is empty,
                          the module name can start with anything.
                          The default is "".

        :param str end: Required end of the package name.
                        If the given str is empty,
                        the module name can end with anything.
                        The default is "".
        """
        modules = []
        paths = [os.path.abspath(path) for path in paths]
        for finder, name, ispkg in pkgutil.iter_modules(paths):
            if skip_controls or (
                not (ispkg and no_pkg)
                and (name.startswith(begin) and name.endswith(end))
            ):
                spec = finder.find_spec(name)
                path = spec.origin
                if path:
                    modules.append(path)

        # Remove paths from importer cache just to be sure
        # that reimports are done correctly.
        for path in paths:
            if path in sys.path_importer_cache:
                del sys.path_importer_cache[path]

        return modules

    @classmethod
    def fetch_folders_site(
        cls, *_, use_pkgutil: bool = True, **kwargs
    ) -> Iterable[str]:
        """
        Fetch site folders.

        This scanner ignores any accepted path as it creates them internally.

        :param bool use_pkgutil:
            If True, site paths and kwargs are passed to
            :meth:`~SearchManager.fetch_folders_pkgutil`,
            otherwise parameters are passed to
            :meth:`~SearchManager.fetch_folders_simple`.
            The default is True.
        """
        paths = site.getsitepackages() + [site.getusersitepackages()]
        if use_pkgutil:
            return cls.fetch_folders_pkgutil(*paths, **kwargs)

        return cls.fetch_folders_simple(*paths, **kwargs)

    @classmethod
    def fetch_entry_point(
        cls, entry_points: Iterable[str], **_
    ) -> Iterable[types.ModuleType]:
        """
        Fetch modules from setuptools' entry points.

        .. warning::
            This method is WIP and misses module controls,
            error handling and testing.
            Its behaviour can change over time and it can be removed too.
        """
        modules = []
        for entry_point in entry_points:
            for entry in pkg_resources.iter_entry_points(entry_point):
                mod = entry.load()
                # self.loaded_modules.append(mod)
                modules.append(mod)
        return modules

    # Manager overrides
    def load_plugin(self, plugin: object, *args, **kwargs) -> Optional[int]:
        """Load search functions."""
        if callable(plugin) and not isinstance(plugin, types.LambdaType):
            uid = super().load_plugin(plugin, *args, **kwargs)
            self._insert_id_into(uid, "name", plugin.__name__)
            return uid
        return None


# TODO: make the manager thread safe
class PluginManager(BaseManager):
    """
    Manage plugins and them loading/unloading.

    It fetches modules and directories to find valid plugins,
    then register and save it.

    It can be configured using the :attr:`~PluginManager.settings` attribute,
    or by passing the settings keys through the constructor.

    :param baselist: A list of classes that is used to check plugin inhiterance
        If one of these is the base of the plugin,
        it will be saved into a dedicated array
        for all the plugins with the same base.
        With this check, all the objects that are not classes
        (instances of :class:`type`), are skipped.
        Default is an empty list, that removes this check.

        .. tip::
           If you want to load any class, use :code:`(object, )`.

    :type baselist: Sequence[type]

    :param namelist: A list of names that plugins must have.
        If one of these is the name of the plugin in the module,
        it will be saved into a dedicated array for
        all the plugins with the same name.
        Default is an empty list, that removes this check.

    :type namelist: Sequence[str]

    .. py:attribute:: searcher
        :type: BaseManager
        :value: SearchManager()
        A plugin manager for storing callables as plugins that
        search for modules.

    .. seealso::
        `The Python import system
        <https://docs.python.org/3/reference/import.html>`_.

        BaseManager
            For a general overview about the
            lplug plugin system and for other parameters.

        SearchManager
            For some module searchers.
    """

    # namedtuples
    PluginSpec = namedlist.create("PluginSpec", "module", "base", "name")
    Settings = BaseManager.Settings.create(
        "Settings",
        "baselist",
        "namelist",
    )

    __slots__ = ("searcher", "__dict__")

    logger = modlogger.getChild("manager")

    def __init__(self, **settings: object):
        _default = {
            "baselist": (),
            "namelist": (),
        }
        _default.update(**settings)
        super().__init__(**_default)
        self.searcher = SearchManager()
        self.searcher.load_plugin(self.searcher.fetch_folders_simple)
        self.searcher.load_plugin(self.searcher.fetch_folders_pkgutil)
        self.searcher.load_plugin(self.searcher.fetch_folders_site)
        self.searcher.load_plugin(self.searcher.fetch_entry_point)

    def __getitem__(self, key: Union[str, type]) -> Optional[Iterable[Any]]:
        """
        Get plugins by given key.

        The key can be:

        - A class to get all plugins that inhiterit from key.

        - A :class:`str` to get all plugins that have key as name.

        This method is similar to :meth:`~.BaseManager.get_plugins_by_category_key`
        except that the category is inferred by key type.
        """
        if isinstance(key, type):
            objects = self.attribute_container.base.get(key)

        elif isinstance(key, str):
            objects = self.attribute_container.name.get(key)

        else:
            raise TypeError(
                "Can't get plugins using a {} as key".format(
                    type(key).__name__,
                )
            )

        if objects is None:
            return None

        return [self.loaded_objects[x][0] for x in objects]

    def load_plugin(
        self,
        plugin: object,
        name: str = None,
        module: types.ModuleType = None,
        uid: int = None,
    ) -> Optional[int]:
        """
        Register a new plugin class and save it into the right category.
        """
        # params processing
        if name is None and isinstance(plugin, type):
            name = plugin.__name__

        # settings variables
        settings = self.settings
        base_enabled = bool(settings.baselist)
        name_enabled = bool(settings.namelist)

        if (not base_enabled or isinstance(plugin, type)) and (
            not name_enabled or name in settings.namelist
        ):
            if base_enabled:
                for base in settings.baselist:
                    if issubclass(plugin, base):
                        uid = super().load_plugin(plugin, name, module, uid)
                        self._insert_id_into(uid, "base", base)
            else:
                uid = super().load_plugin(plugin, name, module, uid)

            self._insert_id_into(uid, "name", name)

            mod_path = getattr(module, "__file__", None)
            if mod_path:
                self._insert_id_into(uid, "module", mod_path)

        return uid

    # module remove
    def remove_module(self, path: str) -> bool:
        """
        Remove all the plugin loaded from module.

        :param path: The path where the module is.

        :return: True if the path was registered, False otherwise.
        """
        if path in self.attribute_container.module:
            for uid in iter(
                self.loaded_objects[x] for x in self.attribute_container.module[path]
            ):
                self.remove_plugin_by_id(uid)

            del self.attribute_container.module[path]

            gc.collect()
            return True

        return False

    def reload_modules(self) -> None:
        """Remove and reload all modules."""
        modules = self.attribute_container.module.copy()
        for path in modules:
            self.remove_module(path)

        for mod in modules:
            self.load_module(mod)

        gc.collect()
