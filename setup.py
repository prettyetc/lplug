#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Lplug setup file."""

from os import path

from setuptools import find_packages, setup

from lplug import __version__

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

# create extras
extras_require = {"test": ['pytest', "hypothesis"]}

# extras_require["all"] = ["yapsy"]

setup(
    name='lplug',
    version=__version__,
    description='Flexible and dynamic plugin system',
    long_description=long_description,
    long_description_content_type='text/x-rst',

    # This should be a valid link to your project's main homepage.
    url='https://gitlab.com/prettyetc/lplug',
    author='trollodel',

    # Classifiers help users find your project by categorizing it.
    # For a list of valid classifiers, see https://pypi.org/classifiers/
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 5 - Production/Stable',

        # Indicate who your project is intended for
        # 'Intended Audience :: Developers',
        # 'Topic :: Software Development :: Build Tools',

        # Pick your license as you wish
        'License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)',

        # Python supported versions
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3 :: Only',

        # System requirements
        'Operating System :: OS Independent',

        # Topics
        'Topic :: Documentation :: Sphinx',

        # Audience
        'Intended Audience :: Developers',
    ],
    keywords='plugin plug-in modules setuptools entry_point yapsy',

    # You can just specify package directories manually here if your project is
    # simple. Or you can use find_packages().
    packages=find_packages(include=("lplug",)),
    python_requires='>=3.4, <4',

    # For an analysis of "install_requires" vs pip's requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    install_requires=["setuptools", 'typing;python_version<"3.5"'],
    extras_require=extras_require,

    # If there are data files included in your packages that need to be
    # installed, specify them here.
    # package_data={
    #     'sample': ['package_data.dat'],
    # },

    # Although 'package_data' is the preferred approach, in some case you may
    # need to place data files outside of your packages. See:
    # http://docs.python.org/3.4/distutils/setupscript.html#installing-additional-files
    #
    # In this case, 'data_file' will be installed into '<sys.prefix>/my_data'
    # data_files=[('my_data', ['data/data_file'])],

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # `pip` to create the appropriate form of executable for the target
    # platform.
    #
    # For example, the following would provide a command called `sample` which
    # executes the function `main` from this package when invoked:
    entry_points={},

    # List additional URLs that are relevant to your project as a dict.
    #
    # This field corresponds to the "Project-URL" metadata fields:
    # https://packaging.python.org/specifications/core-metadata/#project-url-multiple-use
    project_urls={
        'Bug Reports': 'https://gitlab.com/prettyetc/lplug/issues',
        #     'Funding': 'https://donate.pypi.org',
        #     'Say Thanks!': 'http://saythanks.io/to/example',
        "Documentation": "https://prettyetc.gitlab.io/lplug/",
        'Source': 'https://gitlab.com/prettyetc/lplug',
    },
)
