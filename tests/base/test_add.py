#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Test all the BaseManager methods to load plugins."""

from collections.abc import Sequence as SequenceType, Iterable as IterableType
import os
from pathlib import PurePath
import platform
import tempfile
from typing import Sequence, Callable, Mapping
from types import ModuleType
import sys
import uuid

from hypothesis import given, assume
from hypothesis.strategies import (
    lists,
    data,
    iterables,
    sampled_from,
    booleans,
    text,
    one_of,
    dictionaries,
    just,
    binary,
)
import pytest

try:
    from hypothesis.strategies import DataObject
except ImportError:
    # Py3.4 Hypothesis 4.13
    DataObject = data

from lplug import BaseManager

from ..common import (
    ANY_OBJECT,
    module_objects,
    CodeGenerator,
    VALID_IDENTIFIER,
    VALID_FILE_EXT,
    PUBLIC_NAME,
)


if sys.version_info < (3, 6):
    ModuleNotFoundError = ImportError

IS_PYPY = platform.python_implementation() == "PyPy"


class TestAddPluginLoad(object):
    """Test loading plugins through `load_plugin`."""

    @staticmethod
    def check_add_with_filter(
        plugins: Sequence[object], func: Callable[[None, None, object], bool]
    ):
        """"""
        mgr = BaseManager(extra_predicate=func)
        for plugin in plugins:
            uid = mgr.load_plugin(plugin)
            assert (uid is not None) is func(None, None, plugin)

        for plugin in mgr.loaded_objects.values():
            assert func(None, None, plugin[0])

    @given(plugins=lists(ANY_OBJECT, min_size=1), use_uid=booleans())
    def test_load_plugin_simple(self, plugins: Sequence[object], use_uid: bool):
        """
        Test adding plugin through load_plugin.

        Plugins are added without a name and a module.
        """
        mgr = BaseManager()
        for plugin in plugins:
            if use_uid:
                uid_obj = uuid.uuid4()
                uid = mgr.load_plugin(plugin, uid=uid_obj)
                assert uid == uid_obj.int
            else:
                uid = mgr.load_plugin(plugin)

            plugin_pair = mgr.loaded_objects.get(uid)
            assert isinstance(plugin_pair, SequenceType)
            assert len(plugin_pair) == 2
            assert plugin_pair[0] == plugin
            assert plugin_pair[1] == mgr.PluginSpec()

        assert len(plugins) == len(mgr.loaded_objects)
        for plugin in mgr.loaded_objects.values():
            assert plugin[0] in plugins

    @given(
        plugins=lists(ANY_OBJECT, min_size=1),
        data=data(),
        equals=booleans(),
    )
    def test_load_plugin_filter_comparison(
        self, plugins: Sequence[object], data: "DataObject", equals: bool
    ):
        """Test adding plugin through load_plugin with comparison filter."""
        random_plugin = data.draw(sampled_from(plugins))
        if equals:
            func = lambda _, __, x: x == random_plugin
        else:
            func = lambda _, __, x: x != random_plugin

        self.check_add_with_filter(plugins, func)

    @given(
        plugins=lists(ANY_OBJECT, min_size=1),
        data=data(),
        equals=booleans(),
    )
    def test_load_plugin_filter_type(
        self, plugins: Sequence[object], data: "DataObject", equals: bool
    ):
        """Test adding plugin through load_plugin with type filter."""
        random_plugin_type = type(data.draw(sampled_from(plugins)))
        if equals:
            func = lambda _, __, x: isinstance(x, random_plugin_type)
        else:
            func = lambda _, __, x: not isinstance(x, random_plugin_type)

        self.check_add_with_filter(plugins, func)


class TestAddFetchModule(object):
    """Test loading plugins from a module with `fetch_module`."""

    @given(module=module_objects(names=PUBLIC_NAME))
    def test_fetch_module_simple(self, module: ModuleType):
        """
        Test adding plugin through fetch_module.

        Plugins can be listed into __all__ or not.
        """

        def _predicate_checker(mod, name, obj):
            assert mod == module
            assert plugins.get(name, ...) == obj
            return True

        plugins = getattr(module, "___objs")

        mgr = BaseManager(
            use_all=hasattr(module, "__all__"),
            extra_predicate=_predicate_checker,
        )

        assert bool(plugins) is mgr.fetch_module(module)
        assert len(plugins) == len(mgr.loaded_objects)

        # It is difficult to check plugins in order
        # so duplicates are checked more times.
        # However, Hypothesis should create dicts
        # with and without duplicated values
        plugin_values = tuple(plugins.values())
        for plugin in mgr.loaded_objects.values():
            assert plugin[0] in plugin_values

    @given(
        plugins=dictionaries(PUBLIC_NAME, ANY_OBJECT, min_size=1),
        add_invalid_names_to_all=booleans(),
        data=data(),
    )
    def test_fetch_module_attrs_subset(
        self,
        plugins: Mapping[str, object],
        add_invalid_names_to_all: bool,
        data: "DataObject",
    ):
        """
        Test adding plugin through fetch_module and checking a plugin subset.

        :param add_invalid_names_to_all:
            If True, all the names are inserted into __all__
            and only the plugin subset elements are set into the module.
            If False, only the subset names are inserted into __all__
            and all the plugins are set into the module.
        """
        plugins_subset = {}
        for key in data.draw(iterables(sampled_from(sorted(plugins)))):
            plugins_subset[key] = plugins[key]

        module = ModuleType("mod")
        if add_invalid_names_to_all:
            for name, plugin in plugins_subset.items():
                setattr(module, name, plugin)
            module.__all__ = tuple(plugins.keys())
        else:
            for name, plugin in plugins.items():
                setattr(module, name, plugin)
            module.__all__ = tuple(plugins_subset.keys())

        mgr = BaseManager()

        assert bool(plugins_subset) is mgr.fetch_module(module)
        assert len(plugins_subset) == len(mgr.loaded_objects)

        plugin_subset_values = tuple(plugins_subset.values())
        for plugin in mgr.loaded_objects.values():
            assert plugin[0] in plugin_subset_values

    @given(
        module=module_objects(
            names=PUBLIC_NAME, use_all=just(True), object_limits=(1, None)
        ),
        data=data(),
    )
    def test_fetch_module_filter(self, module: ModuleType, data: "DataObject"):
        """Test fetch_module with a filter."""
        plugins = getattr(module, "___objs")

        random_plugin = data.draw(sampled_from(sorted(plugins)))
        func = lambda _, __, x: x == plugins[random_plugin]
        filtered_plugins = tuple(
            filter(
                lambda x: func(None, None, x),
                plugins.values(),
            )
        )

        mgr = BaseManager(extra_predicate=func)
        assert bool(filtered_plugins) is mgr.fetch_module(module)
        assert len(filtered_plugins) == len(mgr.loaded_objects)
        assert all(func(None, None, x[0]) for x in mgr.loaded_objects.values())

    @given(
        module=module_objects(
            names=PUBLIC_NAME, use_all=just(False), object_limits=(0, 5)
        ),
        all_object=ANY_OBJECT.filter(lambda x: not isinstance(x, IterableType)),
    )
    def test_fetch_module_all_fallback(self, module: ModuleType, all_object: object):
        """Test __all__ fallback in fetch_module."""
        plugins = getattr(module, "___objs")
        module.__all__ = all_object

        mgr = BaseManager()

        mgr.fetch_module(module)
        assert len(plugins) == len(mgr.loaded_objects)

        plugin_values = tuple(plugins.values())
        for plugin in mgr.loaded_objects.values():
            assert plugin[0] in plugin_values

    @given(
        module=module_objects(
            use_all=just(True),
            names=text() | text().map(lambda x: "_" + x),
            object_limits=(0, 5),
        )
    )
    def test_fetch_module_private_attributes(self, module: ModuleType):
        """Test fetch_module with only_public = False."""
        plugins = getattr(module, "___objs")

        mgr = BaseManager(only_public=False)

        mgr.fetch_module(module)
        assert len(plugins) == len(mgr.loaded_objects)

        plugin_values = tuple(plugins.values())
        for plugin in mgr.loaded_objects.values():
            assert plugin[0] in plugin_values


class TestAddLoadModule(object):
    """Test loading a module with `load_module`."""

    @given(module=module_objects(names=PUBLIC_NAME, use_all=just(True)))
    def test_load_module_moduletype(self, module: ModuleType):
        """Test load_module by passing a module to it."""
        plugins = getattr(module, "___objs")

        def _predicate_checker(mod, name, obj):
            assert mod == module
            assert plugins.get(name, ...) == obj
            return True

        mgr = BaseManager(
            extra_predicate=_predicate_checker,
        )

        assert bool(plugins) is mgr.load_module(module)
        assert len(plugins) == len(mgr.loaded_objects)

        plugin_values = tuple(plugins.values())
        for plugin in mgr.loaded_objects.values():
            assert plugin[0] in plugin_values

    @given(
        module=module_objects(
            names=VALID_IDENTIFIER,
            objects=ANY_OBJECT.filter(
                lambda x: x != float("inf") and x != float("-inf")
            ),
            use_all=just(True),
            object_limits=(1, None),
            # Sometimes the module is empty
        )
    )
    def test_load_module_simple(self, module: ModuleType):
        """Test load_module by passing the path to a tempfile."""

        def _predicate_checker(module, name, obj):
            assert module.__file__ == path
            assert plugins.get(name, ...) == obj
            return True

        plugins = getattr(module, "___objs")

        path = None
        with tempfile.NamedTemporaryFile(
            delete=False, mode="w+", suffix=".py"
        ) as stream:
            path = stream.name
            CodeGenerator.module_to_file(module, stream)

        # Sometimes the module is empty
        if not module.__all__:
            os.remove(path)
            assume(False)

        # Wrap code to test into a try/finally
        # to guarantee that the temp file is removed.
        try:
            mgr = BaseManager(extra_predicate=_predicate_checker)
            assert mgr.load_module(path)
            assert len(module.__all__) == len(mgr.loaded_objects)

            plugin_values = tuple(plugins.values())
            for plugin in mgr.loaded_objects.values():
                assert plugin[0] in plugin_values
        finally:
            os.remove(path)

    @given(
        module=module_objects(
            names=VALID_IDENTIFIER,
            objects=ANY_OBJECT.filter(
                lambda x: x != float("inf") and x != float("-inf")
            ),
            use_all=just(True),
            object_limits=(1, None),
        ).filter(lambda x: bool(getattr(x, "__all__", None))),
        members_in_external_module=booleans(),
    )
    def test_load_module_is_package(
        self, module: ModuleType, members_in_external_module: bool
    ):
        """Test load_module by passing a path to a package."""
        plugins = getattr(module, "___objs")

        with tempfile.TemporaryDirectory() as dirname:
            init_path = os.path.join(dirname, "__init__.py")
            if members_in_external_module:
                path = None
                with tempfile.NamedTemporaryFile(
                    delete=False, mode="w+", suffix=".py", dir=dirname
                ) as stream:
                    path = stream.name
                    CodeGenerator.module_to_file(module, stream)

                modname = "." + PurePath(path).stem
                codegen = CodeGenerator(open(init_path, "w"))
                codegen.header()
                codegen.import_("*", modname)
                codegen.all_(*module.__all__)
                codegen.file.flush()
            else:
                CodeGenerator.module_to_file(module, open(init_path, "w"))

            assume(bool(module.__all__))

            mgr = BaseManager()
            assert mgr.load_module(dirname, is_folder=True)
            assert len(module.__all__) == len(mgr.loaded_objects)

            plugin_values = tuple(plugins.values())
            for plugin in mgr.loaded_objects.values():
                assert plugin[0] in plugin_values

    # Error testing
    @given(ext=one_of(just(""), VALID_FILE_EXT), data=binary(min_size=1))
    def test_load_module_not_module(self, ext: str, data: bytes):
        """Test load_module by passing a path to an invalid module."""
        path = None
        with tempfile.NamedTemporaryFile(
            delete=False,
            mode="wb",
            suffix=ext,
        ) as stream:
            path = stream.name
            stream.write(data)
        try:
            mgr = BaseManager()
            with pytest.raises(ModuleNotFoundError):
                mgr.load_module(path)
        finally:
            os.remove(path)

    @given(obj=ANY_OBJECT.filter(lambda x: not isinstance(x, (str, bytes))))
    def test_load_module_invalid_object(self, obj: object):
        """Test passing an invalid object to load_module."""
        mgr = BaseManager()
        with pytest.raises(TypeError):
            mgr.load_module(obj)

    # Check sys.modules
    @given(
        module=module_objects(
            names=VALID_IDENTIFIER,
            objects=ANY_OBJECT.filter(
                lambda x: x != float("inf") and x != float("-inf")
            ),
            use_all=just(True),
            object_limits=(1, None),
        ),
    )
    def test_load_module_cleanup_single(self, module: ModuleType):
        """Test load_module if it do cleanup correctly."""
        # This test is quite hacky, basically it instructs
        # the generated module to write to modname.txt its __name__
        # Then that file is read by the test and used to check sys.module

        with tempfile.TemporaryDirectory() as dirname:
            modname_path = os.path.join(dirname, "modname.txt")
            with tempfile.NamedTemporaryFile(
                delete=False, mode="w+", suffix=".py", dir=dirname
            ) as stream:
                path = stream.name
                gen = CodeGenerator.module_to_file(module, stream)
                gen.file_operation(
                    modname_path, "w", "write(__name__)", stream_name="_", flush=True
                )

            # Sometimes the module is empty
            assume(bool(module.__all__))

            mgr = BaseManager()
            assert mgr.load_module(path)
            modname = open(modname_path).read()
            assert not tuple(x for x in sys.modules if x.startswith(modname))

    @given(
        module=module_objects(
            names=VALID_IDENTIFIER,
            objects=ANY_OBJECT.filter(
                lambda x: x != float("inf") and x != float("-inf")
            ),
            use_all=just(True),
            object_limits=(1, None),
        ),
        members_in_external_module=booleans(),
    )
    def test_load_module_cleanup_package(
        self, module: ModuleType, members_in_external_module: bool
    ):
        """Test load_module if it do cleanup correctly."""

        with tempfile.TemporaryDirectory() as dirname:
            modname_path = os.path.join(dirname, "modname.txt")
            init_path = os.path.join(dirname, "__init__.py")

            if members_in_external_module:
                path = None
                with tempfile.NamedTemporaryFile(
                    delete=False, mode="w+", suffix=".py", dir=dirname
                ) as stream:
                    path = stream.name
                    CodeGenerator.module_to_file(module, stream)

                modname = "." + PurePath(path).stem
                codegen = CodeGenerator(open(init_path, "w"))
                codegen.header()
                codegen.import_("*", modname)
                codegen.all_(*module.__all__)
                codegen.file.flush()
            else:
                codegen = CodeGenerator.module_to_file(module, open(init_path, "w"))
            codegen.file_operation(
                modname_path, "w", "write(__name__)", stream_name="_", flush=True
            )
            codegen.file.flush()

            assume(bool(module.__all__))

            mgr = BaseManager()
            assert mgr.load_module(dirname, is_folder=True)
            modname = open(modname_path).read()
            assert not tuple(x for x in sys.modules if x.startswith(modname))
