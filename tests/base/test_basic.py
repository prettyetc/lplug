#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Test basic features of :mod:`lplug.base`."""

from typing import Iterable, Mapping, Set, Sequence, Tuple

from hypothesis import given
from hypothesis.strategies import (
    booleans,
    iterables,
    sampled_from,
    lists,
    dictionaries,
    frozensets,
    tuples,
)

from lplug import BaseManager, namedlist


from ..common import create_manager, VALID_IDENTIFIER, ANY_OBJECT

try:
    from hypothesis.strategies import functions
except ImportError:
    # py 3.4
    from hypothesis.strategies import just

    functions = lambda: just(lambda *_: False)


class TestNamedlist(object):
    """Test namedlist class."""

    @given(name=VALID_IDENTIFIER, attrs=lists(VALID_IDENTIFIER, unique=True))
    def test_create(self, name: str, attrs: Iterable[str]):
        """Test namedlist.create and check if attribute names are added correcly."""
        cls = namedlist.create(name, *attrs)
        assert issubclass(cls, namedlist)
        assert cls.__name__ == name
        assert cls()._attrs == frozenset(attrs)

    @given(
        attrs=lists(
            tuples(VALID_IDENTIFIER, frozensets(VALID_IDENTIFIER)),
            unique=True,
            min_size=1,
        ),
    )
    def test_subclass(self, attrs: Sequence[Tuple[str, Set[str]]]):
        """Test namedlist.create with subclasses and check attributes."""
        cls = namedlist
        for (name, attr_names) in attrs:
            cls = cls.create(name, *attr_names)

        assert issubclass(cls, namedlist)
        resulted_attrs = cls()._attrs
        for (_, attr_names) in attrs:
            for attr_name in attr_names:
                assert attr_name in resulted_attrs

    @given(
        name=VALID_IDENTIFIER,
        attrs=dictionaries(VALID_IDENTIFIER, ANY_OBJECT),
    )
    def test_init(self, name: str, attrs: Mapping[str, object]):
        """Test namedlist init and check attributes."""
        cls = namedlist.create(name, *attrs)
        obj = cls(**attrs)
        assert isinstance(obj, cls)
        assert all(val == getattr(obj, key, ...) for key, val in attrs.items())

    @given(
        name=VALID_IDENTIFIER,
        attrs=dictionaries(VALID_IDENTIFIER, ANY_OBJECT),
    )
    def test_get(self, name: str, attrs: Mapping[str, object]):
        """Test getting attributes by getattr and by subscription."""
        cls = namedlist.create(name, *attrs)
        obj = cls(**attrs)
        for key, val in attrs.items():
            assert getattr(obj, key, None) == val
            assert obj[key] == val

    @given(
        name=VALID_IDENTIFIER,
        attrs=dictionaries(VALID_IDENTIFIER, ANY_OBJECT),
        use_setitem=booleans(),
    )
    def test_set(self, name: str, attrs: Mapping[str, object], use_setitem: bool):
        """Test setting attributes by getattr or by subscription."""
        cls = namedlist.create(name, *attrs)
        obj = cls()
        for key, val in attrs.items():
            if use_setitem:
                obj[key] = val
            else:
                setattr(obj, key, val)
            assert getattr(obj, key) == val

    @given(
        attrs=lists(
            tuples(VALID_IDENTIFIER, frozensets(VALID_IDENTIFIER)),
            unique=True,
            min_size=1,
        ),
    )
    def test_len(self, attrs: Sequence[Tuple[str, Set[str]]]):
        """Test __len__ implementation with subclasses."""
        cls = namedlist
        names = set()
        for (name, attr_names) in attrs:
            cls = cls.create(name, *attr_names)
            names.update(attr_names)

        obj = cls()
        assert len(obj) == len(names)

    # TODO: add test eq
    # TODO: add test iter


class TestManager(object):
    """Do basic tests without plugins on plugin manager."""

    @given(
        booleans(),
        booleans(),
        functions(),
        iterables(
            sampled_from(("use_all", "only_public", "extra_predicate")),
            unique=True,
        ),
    )
    def test_init(
        self,
        use_all: bool,
        only_public: bool,
        extra_predicate,
        remove_keys: Iterable[str],
    ):
        """Test BaseManager creation."""
        settings = dict(
            use_all=use_all,
            only_public=only_public,
            extra_predicate=extra_predicate,
        )
        for key in remove_keys:
            del settings[key]

        create_manager(BaseManager, **settings)
