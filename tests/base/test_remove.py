#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Test plugin remove."""

from collections.abc import Hashable
from typing import Sequence

from hypothesis import given
from hypothesis.strategies import (
    data,
    lists,
    sampled_from,
    iterables,
)

from lplug import BaseManager

from ..common import (
    ANY_OBJECT,
)

try:
    from hypothesis.strategies import DataObject
except ImportError:
    # Py3.4 Hypothesis 4.13
    DataObject = data


UNIQUE_PLUGINS = lists(
    # Allow only hashable objects as plugins
    ANY_OBJECT.filter(lambda x: isinstance(x, Hashable)),
    min_size=1,
    unique=True,
)


class TestRemovePlugin(object):
    """Test removing plugins."""

    # For simplicity, plugins are added only with load_plugin

    @given(
        plugins=UNIQUE_PLUGINS,
        data=data(),
    )
    def test_remove_plugin_by_id_simple(
        self, plugins: Sequence[object], data: DataObject
    ):
        """Test remove_plugin_by_id correctness."""
        mgr = BaseManager()
        for plugin in plugins:
            mgr.load_plugin(plugin)

        uid = data.draw(sampled_from(sorted(mgr.loaded_objects)))
        assert mgr.remove_plugin_by_id(uid)

        assert uid not in mgr.loaded_objects
        assert not mgr.remove_plugin_by_id(uid)

    def test_remove_plugin_by_id_none(self):
        """Test passing None remove_plugin_by_id."""
        mgr = BaseManager()
        assert not mgr.remove_plugin_by_id(None)

    @given(plugins=UNIQUE_PLUGINS, data=data())
    def test_remove_plugin_one(self, plugins: Sequence[object], data: DataObject):
        """Test remove_plugin by removing one plugin."""
        mgr = BaseManager()
        for plugin in plugins:
            mgr.load_plugin(plugin)

        plugin = data.draw(sampled_from(plugins))
        old_len = len(mgr.loaded_objects)

        assert mgr.remove_plugin(plugin)
        assert len(mgr.loaded_objects) == old_len - 1
        assert not mgr.remove_plugin(plugin)

    @given(plugins=UNIQUE_PLUGINS, data=data())
    def test_remove_plugin_multi(self, plugins: Sequence[object], data: DataObject):
        """Test remove_plugin with a subset of plugins."""
        mgr = BaseManager()
        for plugin in plugins:
            mgr.load_plugin(plugin)

        sampled_plugins = data.draw(
            iterables(sampled_from(plugins), unique=True),
        )
        old_len = len(mgr.loaded_objects)

        for plugin in sampled_plugins:
            assert mgr.remove_plugin(plugin)
            assert len(mgr.loaded_objects) == old_len - 1
            assert not mgr.remove_plugin(plugin)
            old_len -= 1

    @given(plugins=UNIQUE_PLUGINS, data=data())
    def test_remove_plugins_simple(self, plugins: Sequence[object], data: DataObject):
        """Test remove_plugins with a subset of plugins."""
        mgr = BaseManager()
        for plugin in plugins:
            mgr.load_plugin(plugin)

        sampled_plugins = data.draw(
            lists(sampled_from(plugins), min_size=1, unique=True).filter(bool)
        )
        old_len = len(mgr.loaded_objects)
        assert mgr.remove_plugins(sampled_plugins)
        assert len(mgr.loaded_objects) == old_len - len(sampled_plugins)
