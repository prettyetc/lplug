#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Common strategies and utilities for tests."""
import datetime
import io
import os
import platform
import re
import types
from typing import Tuple, Optional
from keyword import iskeyword
from string import ascii_letters

from hypothesis.strategies import (
    from_regex,
    text,
    one_of,
    from_type,
    none,
    floats,
    just,
    composite,
    booleans,
    dictionaries,
)

try:
    from hypothesis.strategies import functions
except ImportError:
    # Py3.4 Hypothesis 4.13
    functions = lambda: just(lambda *_: None)

from lplug import BaseManager

__all__ = (
    "VALID_IDENTIFIER",
    "PUBLIC_NAME",
    "VALID_FILENAME",
    "VALID_MODULE_FILENAME",
    "VALID_FILE_EXT",
    "ANY_OBJECT",
    "ANY_REPR_OBJECT",
    "SUBCLASSABLE_TYPES",
    "CodeGenerator",
    "create_manager",
    "module_objects",
)

# Hypothesis constant strategies
VALID_IDENTIFIER = text(ascii_letters + "_", min_size=1).filter(
    lambda x: not (x.startswith("_") or iskeyword(x))
)
PUBLIC_NAME = text(min_size=1).filter(lambda x: not x.startswith("_") and "\0" not in x)

BLACKLISTED_CHARS = [os.path.sep, r"\x00"]

if platform.system() == "Windows":
    BLACKLISTED_CHARS += [
        r"\x01-\x08",
        r"\x0b-\x0c",
        r"\x0e-\x1f",
    ]
    BLACKLISTED_CHARS += ["\\" + x for x in '"*$?:|t/rn']
    BLACKLISTED_CHARS += list("<>")

REGEX_BLACKLISTED_CHARS = re.compile(
    "^[^" + "".join(BLACKLISTED_CHARS) + "]+$",
    re.M,
)
VALID_FILENAME = from_regex(
    REGEX_BLACKLISTED_CHARS,
    fullmatch=True,
).filter(lambda x: len(x) < 32)
VALID_MODULE_FILENAME = VALID_FILENAME.filter(
    lambda x: not ("." in x or x.startswith("_"))
)

VALID_FILE_EXT = one_of(
    from_regex(
        REGEX_BLACKLISTED_CHARS,
        fullmatch=True,
    )
    .filter(lambda x: len(x) < 16)
    .map(lambda x: "." + x),
    just("."),
)

ANY_OBJECT = one_of(
    from_type(bool),
    from_type(int),
    from_type(str),
    from_type(bytes),
    from_type(list),
    from_type(tuple),
    from_type(dict),
    from_type(frozenset),
    from_type(set),
    from_type(type),
    floats(allow_nan=False),
    functions(),
    none(),
)

ANY_REPR_OBJECT = one_of(
    from_type(bool),
    from_type(int),
    from_type(str),
    from_type(bytes),
    from_type(list),
    from_type(tuple),
    from_type(dict),
    from_type(frozenset),
    from_type(set),
    floats(allow_nan=False),
    none(),
)

SUBCLASSABLE_TYPES = from_type(type).filter(
    lambda x: x
    not in (
        type(None),
        type(NotImplemented),
        type(...),
        bool,
        range,
        slice,
        memoryview,
        datetime.timezone,
        types.FunctionType,
    )
)


# Utils
class CodeGenerator(object):
    """Create chunks of code."""

    __all__ = ("file", "ind")

    def __init__(self, file: io.IOBase = None):
        super().__init__()
        self.file = file
        self.ind = 0

    @classmethod
    def module_to_file(
        cls,
        module: types.ModuleType,
        file: io.IOBase,
        move_all: bool = True,
    ) -> "CodeGenerator":
        """Convert a module objects into a script file."""
        gen = cls(file)
        gen.header()
        all_names = list(module.__all__)
        for name in module.__all__:
            obj_repr = repr(getattr(module, name))
            if obj_repr.startswith("<"):
                all_names.remove(name)
            else:
                gen.assign(name, obj_repr)

        if move_all:
            gen.all_(*all_names)
            module.__all__ = all_names

        gen.file.flush()
        return gen

    def _write(self, content: str) -> str:
        new_content = ""
        for line in content.splitlines():
            new_content += ("    " * self.ind) + line.lstrip("\t ") + "\n"
        content = new_content

        if self.file:
            self.file.write(content)
        return content

    def header(self) -> str:
        """Generate the script header."""
        retstr = """
        #!/usr/bin/env python3
        # -*- coding: utf-8 -*-
        """.strip(
            "\n "
        )

        return self._write(retstr)

    def assign(self, name: str, value: str) -> str:
        """Create an assignement."""
        retstr = "{} = {}\n".format(name, value)
        return self._write(retstr)

    def import_(self, name, from_: str = None, as_: str = None) -> str:
        """Add an import."""
        retstr = ""

        if from_:
            retstr += "from {} import {}".format(from_, name)
        else:
            retstr += "import " + name

        if as_:
            retstr += " as " + as_
        retstr += "\n"

        return self._write(retstr)

    def type_(self, name: str, bases: tuple, attrs: dict, varname: str = None) -> str:
        """Create a class with type()."""
        if len(bases) == 1:
            bases = bases[0].__name__ + ","
        else:
            bases = ", ".join(x.__name__ for x in bases)

        if varname is None:
            varname = name
        retstr = "{} = type({!r}, ({!s}), {!r})\n".format(varname, name, bases, attrs)

        return self._write(retstr)

    def all_(self, *names):
        """Generate the __all__ attribute."""
        if names:
            retstr = "__all__ = ({},)\n".format(", ".join(repr(x) for x in names))
        else:
            retstr = "__all__ = ()"
        return self._write(retstr)

    def file_operation(
        self,
        path: str,
        mode: str,
        operation: str,
        stream_name: str = "stream",
        variable_name: str = None,
        flush: bool = False,
    ):
        """Do a file i/o operation using open()."""
        self._write("with open({!r}, {!r}) as {}:".format(path, mode, stream_name))
        self.ind += 1
        operation = "{}.{}".format(stream_name, operation)
        if variable_name:
            self.assign(variable_name, operation)
        else:
            self._write(operation)
        if flush:
            self._write("{}.flush()".format(stream_name))
        self.ind -= 1


def create_manager(cls, check_settings: bool = True, **settings):
    """Create an instance of given BaseManager subclass with given settings."""
    assert issubclass(cls, BaseManager)
    manager = cls(**settings)
    if check_settings:
        for given in settings:
            assert settings[given] == getattr(manager.settings, given, None)
    return manager


# Composite strategies
@composite
def module_objects(
    draw,
    names=text(min_size=1),
    objects=ANY_OBJECT,
    use_all=booleans(),
    module_name=just("mod"),
    object_limits: Tuple[Optional[int]] = (0, None),
) -> types.ModuleType:
    """
    Create a module object.

    :param names: A strategy to generated attribute names.

    :param objects: A strategy to generate random objects.

    :param module_name: A strategy to generate the module name.

    :param use_all: A strategy to decide if using __all__ or not.
    """
    module = types.ModuleType(draw(module_name))
    objects_map = draw(
        dictionaries(
            names, objects, min_size=object_limits[0], max_size=object_limits[1]
        ),
    )
    # Put generated objects into this attribute
    module.___objs = objects_map

    for name, plugin in objects_map.items():
        setattr(module, name, plugin)
    if draw(use_all):
        module.__all__ = tuple(objects_map.keys())

    return module
