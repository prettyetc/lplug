#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import logging

from hypothesis import given, settings
from hypothesis.strategies import dictionaries, lists, iterables, tuples

from lplug import PluginManager

from ..common import PUBLIC_NAME, SUBCLASSABLE_TYPES


def setup_module(_):
    """Setup the logger."""
    logger = logging.getLogger("lplug")
    # remove handlers
    for handler in logger.handlers.copy():
        logger.removeHandler(handler)


class TestClassAdd(object):
    """Test loading plugins that should subclass specified classes."""

    @given(members=lists(PUBLIC_NAME, unique=True))
    def test_base_any(self, members: list):
        """Test adding classes that inhiterit from object."""
        plugins = tuple(type(member, (object,), {}) for member in members)
        mgr = PluginManager(baselist=(object,))
        for plugin in plugins:
            assert mgr.load_plugin(plugin)

        category = mgr.get_plugins_by_category_key("base", object)
        loaded_plugins = tuple(x[0] for x in mgr.loaded_objects.values())
        for plugin in plugins:
            assert plugin in category
            assert plugin in loaded_plugins

    @settings(deadline=300)
    @given(
        members=dictionaries(
            SUBCLASSABLE_TYPES,  # .filter(lambda x: x != object),
            iterables(PUBLIC_NAME, unique=True),
        )
    )
    def test_base_one(self, members: dict):
        """Test adding classes that inhiterit from a specific class."""
        members = {
            type_: tuple(type(name, (type_,), {}) for name in names)
            for type_, names in members.items()
        }
        mgr = PluginManager(baselist=tuple(members))

        for sublist in members.values():
            for plugin in sublist:
                assert mgr.load_plugin(plugin)

        loaded_plugins = tuple(x[0] for x in mgr.loaded_objects.values())
        for type_, sublist in members.items():
            category = mgr.get_plugins_by_category_key("base", type_)
            for plugin in sublist:
                assert plugin in loaded_plugins
                assert plugin in category

    @settings(deadline=300)
    @given(
        members=lists(
            tuples(
                PUBLIC_NAME,
                lists(SUBCLASSABLE_TYPES, unique=True, min_size=1),
            )
        )
    )
    def test_base_multi(self, members: list):
        """Test adding classes that inhiterit several specific classes."""
        plugins = []
        type_map = {}
        for (name, types) in members:
            try:
                cls = type(name, tuple(types), {})
            except TypeError:
                # Some types can't be combined with each other
                pass
            else:
                plugins.append(cls)
                for type_ in types:
                    type_map.setdefault(type_, [])
                    type_map[type_].append(cls)

        mgr = PluginManager(baselist=tuple(type_map))

        for plugin in plugins:
            assert mgr.load_plugin(plugin)

        loaded_plugins = tuple(x[0] for x in mgr.loaded_objects.values())
        for type_, sublist in type_map.items():
            category = mgr.get_plugins_by_category_key("base", type_)
            for plugin in sublist:
                assert plugin in loaded_plugins
                assert plugin in category
