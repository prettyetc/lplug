#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Test scan folder with :class:`SearchManager`."""
import os
import logging
import tempfile

from hypothesis import given
from hypothesis.strategies import (
    lists,
    integers,
    booleans,
)

from lplug import SearchManager

from ..common import VALID_MODULE_FILENAME

EMPTY_MODULE = b"""#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""


def setup_module(_):
    """Setup the logger."""
    logger = logging.getLogger("lplug")
    # remove handlers
    for handler in logger.handlers.copy():
        logger.removeHandler(handler)


class TestFetchFolderSimple(object):
    """Test :meth:`.SearchManager.fetch_folders_simple` method."""

    @given(integers(min_value=1, max_value=10))
    def test_fetch_folder_modules(self, modules_count: int):
        """Test fetch_folders_simple with modules."""
        with tempfile.TemporaryDirectory() as dirname:
            modnames = set()
            for _ in range(modules_count):
                with tempfile.NamedTemporaryFile(
                    suffix=".py", dir=dirname, delete=False
                ) as stream:
                    stream.write(EMPTY_MODULE)
                    modnames.add(stream.name)
            result = SearchManager.fetch_folders_simple((dirname,))
            assert len(result) == len(modnames)
            assert all(x in modnames for x in result)

        # TODO: Create other tests for fetch_folders_simple


class TestFetchFolderPkgutil(object):
    """Test :meth:`.SearchManager.fetch_folders_simple` method."""

    @given(integers(min_value=1, max_value=10))
    def test_fetch_folder_pkgutil_modules(self, modules_count: int):
        """Test fetch_folders_pkgutil with modules."""
        with tempfile.TemporaryDirectory() as dirname:
            modnames = set()
            for _ in range(modules_count):
                with tempfile.NamedTemporaryFile(
                    suffix=".py", dir=dirname, delete=False
                ) as stream:
                    stream.write(EMPTY_MODULE)
                    modnames.add(stream.name)
            result = SearchManager.fetch_folders_pkgutil((dirname,))
            assert len(result) == len(modnames)
            assert all(x in modnames for x in result)

    @given(
        pkgnames=lists(VALID_MODULE_FILENAME, min_size=1, unique=True),
        extra_module=booleans(),
    )
    def test_fetch_folder_pkgutil_packages(self, pkgnames: list, extra_module: bool):
        """Test fetch_folders_pkgutil with packages."""
        with tempfile.TemporaryDirectory() as dirname:
            for pkgdir in pkgnames:
                pkgdir = os.path.join(dirname, pkgdir)
                os.makedirs(pkgdir)
                with open(os.path.join(pkgdir, "__init__.py"), "wb") as stream:
                    stream.write(EMPTY_MODULE)

                if extra_module:
                    with tempfile.NamedTemporaryFile(
                        suffix=".py", dir=pkgdir, delete=False
                    ) as stream:
                        stream.write(EMPTY_MODULE)

            result = SearchManager.fetch_folders_pkgutil((dirname,))
            assert len(result) == len(pkgnames)
            assert all(
                os.path.join(dirname, x, "__init__.py") in result for x in pkgnames
            )

    @given(modnames=lists(VALID_MODULE_FILENAME | integers(), min_size=1, unique=True))
    def test_fetch_folder_pkgutil_mixed(self, modnames: list):
        """Test fetch_folders_pkgutil with both modules and packages."""
        with tempfile.TemporaryDirectory() as dirname:
            for i, modname in enumerate(modnames.copy()):
                # Ints act like a placeholder for modules
                # and can have different values.
                if isinstance(modname, int):
                    # Module
                    with tempfile.NamedTemporaryFile(
                        suffix=".py", dir=dirname, delete=False
                    ) as stream:
                        stream.write(EMPTY_MODULE)
                        modnames[i] = stream.name
                else:
                    # Package
                    pkgdir = os.path.join(dirname, modname)
                    os.makedirs(pkgdir)
                    with open(os.path.join(pkgdir, "__init__.py"), "wb") as stream:
                        stream.write(EMPTY_MODULE)
                        modnames[i] = stream.name

            result = SearchManager.fetch_folders_pkgutil((dirname,))
            assert len(result) == len(modnames)
            assert all(x in modnames for x in result)

    @given(modnames=lists(VALID_MODULE_FILENAME | integers(), min_size=1, unique=True))
    def test_fetch_folder_pkgutil_nopkg(self, modnames: list):
        """Test fetch_folder_pkgutil with no_pkg=True."""
        with tempfile.TemporaryDirectory() as dirname:
            for i, modname in enumerate(modnames.copy()):
                # Ints act like a placeholder for modules
                # and can have different values.
                if isinstance(modname, int):
                    # Module
                    with tempfile.NamedTemporaryFile(
                        suffix=".py", dir=dirname, delete=False
                    ) as stream:
                        stream.write(EMPTY_MODULE)
                        modnames[i] = stream.name
                else:
                    # Package
                    pkgdir = os.path.join(dirname, modname)
                    os.makedirs(pkgdir)
                    with open(os.path.join(pkgdir, "__init__.py"), "wb") as stream:
                        stream.write(EMPTY_MODULE)
                        modnames[i] = stream.name

            result = SearchManager.fetch_folders_pkgutil((dirname,), no_pkg=True)
            assert all(
                not (x.endswith("__init__.py") and x in result) for x in modnames
            )
